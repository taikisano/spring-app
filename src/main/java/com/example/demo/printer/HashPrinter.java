package com.example.demo.printer;

import java.util.Map;

public class HashPrinter implements Printer {

    @Override
    public String print(Map<String, String> data) {

        StringBuilder sb = new StringBuilder();

        for (String key : data.keySet()) {
            sb.append(key);
            sb.append("=>");
            sb.append(data.get(key));
            sb.append("\r\n");
        }
        return sb.toString();
    }    
}
