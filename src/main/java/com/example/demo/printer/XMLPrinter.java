package com.example.demo.printer;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class XMLPrinter implements Printer {

    @Override
    public String print(Map<String, String> data) {

        StringBuilder sb = new StringBuilder();

        for (String key : data.keySet()) {
            sb.append("<");
            sb.append(key);
            sb.append(">");
            sb.append(data.get(key));
            sb.append("</ ");
            sb.append(key);
            sb.append(">");
            sb.append("\r\n");
        }
        return sb.toString();
    }    

}
