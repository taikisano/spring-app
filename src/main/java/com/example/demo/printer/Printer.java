package com.example.demo.printer;

import java.util.Map;

public interface Printer {    
    String print(Map<String, String> data);
}
