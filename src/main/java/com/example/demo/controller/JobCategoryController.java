package com.example.demo.controller;

import com.example.demo.entity.JobCategory;
import com.example.demo.mapper.JobCategoryMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("job-categories")
public class JobCategoryController {
    
    @Autowired
    private JobCategoryMapper jobCategoryMapper;

    @GetMapping("{id}")
    public String show(@PathVariable Integer id, Model model) {
        JobCategory jobCategory = jobCategoryMapper.findById(id);
        model.addAttribute("jobCategory", jobCategory);
        return "jobCategories/show";
    }
}
