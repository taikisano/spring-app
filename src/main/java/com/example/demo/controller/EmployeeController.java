package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.example.demo.entity.Employee;
import com.example.demo.entity.JobCategory;
import com.example.demo.form.EmployeeForm;
import com.example.demo.mapper.EmployeeMapper;
import com.example.demo.mapper.JobCategoryMapper;
import com.example.demo.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    HttpSession session;

    @Autowired
    private EmployeeService employeeService;
    
    @Autowired
    private JobCategoryMapper jobCategoryMapper;

    @ModelAttribute
    EmployeeForm setupForm() {
        return new EmployeeForm();
    }

    @GetMapping
    public String index(Model model) {

        String message = (String)session.getAttribute("message");
        session.removeAttribute("message");
        model.addAttribute("message", message);

        List<Employee> employees = employeeService.findAll();
        model.addAttribute("employees", employees);
        return "employees/index";
    }

    @GetMapping("{id}")
    public String show(@PathVariable int id, Model model) {
        Employee employee = employeeService.findById(id);
        model.addAttribute("employee", employee);
        return "employees/show";
    }

    @GetMapping("create")
    public String create(Model model) {
        List<JobCategory> jobCategories = jobCategoryMapper.findAll();
        model.addAttribute("jobCategories", jobCategories);
        return "employees/create";
    }

    @PostMapping("insert")
    public String insert(@Validated EmployeeForm form, BindingResult result, Model model) {
        
        if (result.hasErrors()) {
            return create(model);
        }

        Employee employee = new Employee();
        employee.setName(form.getName());
        employee.getJobCategory().setId(form.getJobCategoryId());

        // DBに登録
        employeeService.insert(employee);

        return "redirect:/employees";
    }

    @GetMapping("edit")
    public String edit(@RequestParam Integer id, Model model, EmployeeForm form) {

        List<JobCategory> jobCategories = jobCategoryMapper.findAll();
        model.addAttribute("jobCategories", jobCategories);

        Employee employee = employeeService.findById(id);
        form.setJobCategoryId(employee.getJobCategory().getId());
        form.setName(employee.getName());

        return "employees/edit";
    }

    @PostMapping("update")
    public String update(@RequestParam Integer id,
                         @Validated EmployeeForm form, 
                         BindingResult result, 
                         Model model) {

        if (result.hasErrors()) {
            return edit(id, model, form);
        }

        Employee employee = employeeService.findById(id);
        employee.getJobCategory().setId(form.getJobCategoryId());
        employee.setName(form.getName());

        employeeService.update(employee);

        session.setAttribute("message", "更新が完了しました");

        return "redirect:/employees";
    }
}
