package com.example.demo.controller;
import java.util.Arrays;
import java.util.List;
import com.example.demo.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
    
    private List<User> users = Arrays.asList(
        new User("John", "john@example.com"),
        new User("Paul", "paul@example.com"),
        new User("Taro", "taro@example.com")
    );

    @RequestMapping("/users")
    public String index(Model model) {
        model.addAttribute("users", users);
        return "users/index";
    }

}