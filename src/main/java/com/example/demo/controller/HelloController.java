package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import com.example.demo.printer.Printer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
    
    // 代入するインスタンスを変えれば、動作を変えられる（ポリモフィズム）
    @Autowired
    Printer printer;

    @RequestMapping("/hello")
    public String index(Model model) {
        
        Map<String, String> user = new HashMap<>();
        user.put("name", "John");
        user.put("email", "john@example.com");

        String text = printer.print(user);

        model.addAttribute("text", text);
        return "hello";
    }
}
