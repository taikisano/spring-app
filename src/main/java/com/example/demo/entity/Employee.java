package com.example.demo.entity;

import lombok.Data;

@Data
public class Employee {

    public Employee() {
        this.jobCategory = new JobCategory();
    }

    private int id;
    private String name;
    private JobCategory jobCategory;
}
