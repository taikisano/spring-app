package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Employee;
import com.example.demo.mapper.EmployeeMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;

    public List<Employee> findAll() {
        List<Employee> employees = employeeMapper.findAll();
        return employees;
    }

    public Employee findById(int id) {
        Employee employee = employeeMapper.findById(id);
        return employee;
    }

    public void insert(Employee employee) {
        employeeMapper.insert(employee);
    }

    public void update(Employee employee) {
        employeeMapper.update(employee);
    }

    public void delete(int id) {
        employeeMapper.delete(id);
    }
}
