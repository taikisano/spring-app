package com.example.demo.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class EmployeeForm {
    
    @NotNull
    private Integer jobCategoryId;

    @NotEmpty
    // @Size(min=1, max=10)
    private String name;
}
