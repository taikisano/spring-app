package com.example.demo.mapper;

import java.util.List;

import com.example.demo.entity.JobCategory;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JobCategoryMapper {
    List<JobCategory> findAll();
    JobCategory findById(int id);
}
